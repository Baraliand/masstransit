﻿using MassTransit;
using MasstransitDemo.Consumer;
using MasstransitDemo.Shared;
using Microsoft.Extensions.Hosting;
using System.Reflection;

var builder = Host.CreateDefaultBuilder(args);

builder.ConfigureServices((hostContext, services) =>
{
    services.AddMassTransit(x =>
    {
        x.AddServiceBusMessageScheduler();
        x.SetKebabCaseEndpointNameFormatter();
        x.AddConsumer<NotificationCreatedConsumer>();
        //x.UsingAzureServiceBus((context, cfg) =>
        //{
        //    cfg.Host("Endpoint=sb://ent-az-we-dev-cdh-sbus-004.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=SB1lK/LxK0KOBVF7IgLEtpBd6Fd6c5mWo+ASbKXfR2I=");
        //    cfg.UseServiceBusMessageScheduler();
        //    // Subscribe to OrderSubmitted directly on the topic, instead of configuring a queue
        //    cfg.ReceiveEndpoint("queue2", e =>
        //    {
        //        e.ConfigureConsumer<NotificationCreatedConsumer>(context);
        //    });
        //    cfg.ConfigureEndpoints(context);
        //});
        x.UsingRabbitMq((context, cfg) =>
        {
            cfg.Host("Endpoint=sb://ent-az-we-dev-cdh-sbus-004.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=SB1lK/LxK0KOBVF7IgLEtpBd6Fd6c5mWo+ASbKXfR2I=");
            cfg.UseServiceBusMessageScheduler();
            // Subscribe to OrderSubmitted directly on the topic, instead of configuring a queue
            cfg.ReceiveEndpoint("queue2", e =>
            {
                e.ConfigureConsumer<NotificationCreatedConsumer>(context);
            });
            cfg.ConfigureEndpoints(context);
        });
    });
});

var app = builder.Build();

app.Run();