﻿using MassTransit;

namespace MasstransitDemo.Api
{
    public class SampleConsumer : IConsumer<string>
    {
        public async Task Consume(ConsumeContext<string> context)
        {
            var message = context.Message;

            Console.WriteLine($"Message reçu: {message}");
        }
    }
}
