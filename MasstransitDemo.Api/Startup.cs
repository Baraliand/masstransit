﻿using MassTransit;

namespace MasstransitDemo.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration;

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();

            services.AddMassTransit(busConfigurator =>
            {
                busConfigurator.SetKebabCaseEndpointNameFormatter();
                busConfigurator.UsingAzureServiceBus((context, busFactoryConfigurator) =>
                {
                    var azureServiceBusConfig = Configuration.GetSection("AzureServiceBus");
                    var connectionString = azureServiceBusConfig["ConnectionString"];
                    var queueName = azureServiceBusConfig["QueueName"];

                    busFactoryConfigurator.Host(connectionString);
                });
                //busConfigurator.UsingRabbitMq((context, busFactoryConfigurator) =>
                //{
                //    var azureServiceBusConfig = Configuration.GetSection("AzureServiceBus");
                //    var connectionString = azureServiceBusConfig["ConnectionString"];
                //    var queueName = azureServiceBusConfig["QueueName"];

                //    busFactoryConfigurator.Host(connectionString);
                //});
            });


            services.AddMassTransitHostedService();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
