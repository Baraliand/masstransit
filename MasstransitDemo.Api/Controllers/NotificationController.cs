﻿using MassTransit;
using MasstransitDemo.Api.Models;
using MasstransitDemo.Shared;
using Microsoft.AspNetCore.Mvc;

namespace MasstransitDemo.Api.Controllers;

[Route("api/[controller]")]
[ApiController]
public class NotificationController : ControllerBase
{
    public readonly ISendEndpointProvider _sendEndpointProvider;
    public readonly Uri _serviceAddress;

    public NotificationController(ISendEndpointProvider sendEndpointProvider)
    {
        this._sendEndpointProvider = sendEndpointProvider;
        _serviceAddress = new Uri("sb://ent-az-we-dev-cdh-sbus-004.servicebus.windows.net/queue2");
    }

    [HttpPost]
    public async Task<IActionResult> Notify(NotificationDto notificationDto)
    {
        var endpoint = await _sendEndpointProvider.GetSendEndpoint(_serviceAddress);
        await endpoint.Send<INotificationCreated>(new {
            NotificationDate = notificationDto.NotificationDate,
            NotificationMessage = notificationDto.NotificationMessage,
            NotificationType = notificationDto.NotificationType
        });

        return Ok();
    }
}